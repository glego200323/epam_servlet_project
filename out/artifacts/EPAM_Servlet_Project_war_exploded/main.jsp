<%@ page import="model.entity.User" %>
<%@ page import="model.service.UserService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" session="true" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Language" />
<html>
  <head>
      <meta charset="UTF-8"/>
      <title>EPAM_Servlet_Project</title>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
  </head>
  <body >
  <div>
      <div class="container-fluid py-3" style="height: 80px; background-color: yellowgreen;">
          <div class="d-flex flex-column flex-md-row align-items-center" >
              <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
                  <c:if test="${user != null}">
                      <c:if test="${user.getRole() == 'admin'}">
                          <input onclick="window.location.href='/admin?language=${language}'" type="button" class="me-3 py-2 rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" value="<fmt:message key="admin-user-button"/>">
                      </c:if>
                      <c:if test="${user.getRole() == 'user'}">
                          <input onclick="window.location.href='/user?language=${language}'" class="me-3 py-2 rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" type="button" value="<fmt:message key="log-in-message"/>">
                      </c:if>
                      <c:if test="${user.getRole() == 'ban'}">
                          <input type="button" style="background-color: wheat; height: 40px; border-radius: 5px ;" class="me-3 py-2 rounded ac" value="<fmt:message key="ban-message"/>">
                      </c:if>
                      <input class="me-3 py-2 rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" type="button" onclick="window.location.href='/logOut?language=${language}'" type="button" value="<fmt:message key="log-out-button"/>">
                  </c:if>
                  <c:if test="${user == null}">
                      <form method="post" action="/auth?language=${language}">
                          <input type="hidden" name="viewed" value="/main?language=${language}">
                          <input class="me-3 py-2 rounded" name="authLogin" style="height: 40px;" type="text" size="10" placeholder="login">
                          <input class="me-3 py-2 rounded" name="authPassword" style="height: 40px;" type="password" size="10" placeholder="password">
                          <input type="submit" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="log-in-button"/>">
                      </form>
                  </c:if>
                  <input onclick="window.location.href='/reg?language=${language}'" type="button" class="me-3 py-2 rounded" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="registration-button"/>">
                  <form>
                      <select class="form-control me-3 py-2 c" style="width: 120px; height: 40px; background-color: wheat; border-color: black; border-width: 2px" name="language" onchange="submit()">
                          <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                          <option value="uk" ${language == 'uk' ? 'selected' : ''}>Українська</option>
                      </select>
                  </form>
              </nav>
          </div>
      </div>
      <div class="container">
          <div class="row">
              <form>
                  <table class="table table-hover">
                      <thead>
                      <tr class="active">
                          <th>
                              <div class="col">
                                  <fmt:message key="table-faculty-name"/>
                                  <input onclick="window.location.href='/main?language=${language}&sort=name&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                  <input onclick="window.location.href='/main?language=${language}&sort=nameDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                              </div>
                          </th>
                          <th>
                              <div class="col">
                                  <fmt:message key="table-faculty-allPlaces"/>
                                  <input onclick="window.location.href='/main?language=${language}&sort=allPlaces&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                  <input onclick="window.location.href='/main?language=${language}&sort=allPlacesDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                              </div>

                          </th>
                          <th>
                              <div class="col">
                                  <fmt:message key="table-faculty-freePlaces"/>
                                  <input onclick="window.location.href='/main?language=${language}&sort=freePlaces&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                  <input onclick="window.location.href='/main?language=${language}&sort=freePlacesDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                              </div>
                          </th>
                      </tr>
                      </thead>
                      <tbody>
                          <c:forEach items="${list}" var="faculty">
                              <tr>
                                  <td><c:out value="${faculty.name}" /></td>
                                  <td><c:out value="${faculty.allPlaces}" /></td>
                                  <td><c:out value="${faculty.freePlaces}" /></td>
                                  <c:if test="${user.role == 'user'}">
                                  <td>
                                      <c:set var="contains" value="false" />
                                      <c:forEach var="item" items="${userFacultyIdList}">
                                      <c:if test="${item eq faculty.id}">
                                          <c:set var="contains" value="true" />
                                      </c:if>
                                      </c:forEach>
                                      <c:if test="${contains != true}">
                                          <input class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" type="button" value="<fmt:message key="registration-button"/>" onclick="window.location.href='/userRegFaculty?facultyId=${faculty.id}'">
                                      </c:if>
                                  <td>
                                  </c:if>
                                  <c:if test="${user.role == 'admin'}">
                                  <td>
                                    <input type="button" onclick="window.location.href='/delFac?facultyId=${faculty.id}'" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="delete-button"/>">
                                    <input type="button" onclick="window.location.href='/edit?facultyId=${faculty.id}'" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="edit-button"/>">
                                    <input type="button" onclick="window.location.href='/final?facultyId=${faculty.id}'" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="finalize-button"/>">
                                  <td>
                                  </c:if>
                              </tr>
                          </c:forEach>
                      </tbody>
                  </table>
              </form>
                  <div class="row">
                      <div class="col d-flex justify-content-center">
                          <input type="button" name="page" value="<<" onclick="window.location.href='/main?language=${language}&page=<%=pref(request.getParameter("page"))%>&sort=<%=request.getParameter("sort")%>'"/>
                          <input type="button" name="page" value=">>" onclick="window.location.href='/main?language=${language}&page=<%=next(request.getParameter("page"))%>&sort=<%=request.getParameter("sort")%>'"/>
                      </div>
                  </div>
          </div>
      </div>
  </div>
  </body>
</html>
<%!
    int pref(String page){
        if(page == null ){
            return 1;
        }
        int pageNum = Integer.parseInt(page);
        if(pageNum <= 1)
        {
            return 1;
        }
        return pageNum - 1;
    }
    int next(String page){
        if(page == null){
            return 2;
        }
        int pageNum = Integer.parseInt(page);
        return pageNum + 1;
    }
    int current(String page){
        if(page == null){
            return 1;
        }
        return Integer.parseInt(page);
    }
%>