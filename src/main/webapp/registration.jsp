<%@ page import="controller.command.ActionMap" %>
<%@ page import="javax.swing.*" %>
<%@ page import="model.dao.UserDAO" %>
<%@ page import="model.entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" session="true" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Language" />

<html>
<head>
    <meta charset="UTF-8"/>
    <title>EPAM_Servlet_Project</title>
    <link rel="stylesheet" href="static/css/error.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src="static/js/error.js"></script>
</head>
<body>
<div>
    <div class="container-fluid py-3" style="height: 80px; background-color: yellowgreen;">
        <div class="d-flex flex-column flex-md-row align-items-center" >
            <input onclick="window.location.href='/main?language=${language}'" type="button" class="rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" value="<fmt:message key="main-button"/>">
            <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
                <c:if test="${user != null}">
                    <input class="me-3 py-2 rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" type="button" value="<fmt:message key="log-in-message"/>">
                </c:if>
                <c:if test="${user == null}">
                    <form method="post" action="/auth?language=${language}">
                        <input type="hidden" name="viewed" value="/reg?language=${language}">
                        <input class="me-3 py-2 rounded" name="authLogin" style="height: 40px;" type="text" size="10" placeholder="login">
                        <input class="me-3 py-2 rounded" name="authPassword" style="height: 40px;" type="password" size="10" placeholder="password">
                        <input type="submit" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="log-in-button"/>">
                    </form>
                </c:if>
                <form>
                    <select class="form-control me-3 py-2 c" style="width: 120px; height: 40px; background-color: wheat; border-color: black; border-width: 2px" name="language" onchange="submit()">
                        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                        <option value="uk" ${language == 'uk' ? 'selected' : ''}>Українська</option>
                    </select>
                </form>
            </nav>
        </div>
    </div>
</div>
<h1></h1>
<form method="post" action="/regUser?language${language}">
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="name-label"/></label>
            <input type="text" class="rounded form-control" id="name" name="name" required="required" pattern="^([A-Z][a-z\-\']{1,50})|([А-ЯЁIЇҐЄ][а-яёіїґє\-\']{1,50})$">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="email-label"/></label>
            <input type="email" class="rounded form-control" id="email" name="email" required="required">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="city-label"/></label>
            <input type="text" class="rounded form-control" id="city" name="city" required="required" pattern="^([A-Z][a-z\-\']{1,50})|([А-ЯЁIЇҐЄ][а-яёіїґє\-\']{1,50})$">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="district-label"/></label>
            <input type="text" class="rounded form-control" id="district" name="district" required="required" pattern="^([A-Z][a-z\-\']{1,50})|([А-ЯЁIЇҐЄ][а-яёіїґє\-\']{1,50})$">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="school-label"/></label>
            <input type="text" class="rounded form-control" id="school" name="school" required="required" pattern="^([A-Z][a-z\-\'][0-9]{1,50})|([А-ЯЁIЇҐЄ][а-яёіїґє\-\']{1,50})$">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5 validation-container" <c:if test="${loginError != null}">data-error="<fmt:message key="message-login-error"/>"</c:if>>
            <label class=""><fmt:message key="login-label"/></label>
            <input type="text" class="rounded form-control" id="login" name="login" required="required" pattern="^(?=.*[0-9])(?=.*[a-zA-Z-_@#!%$^&*])(?=\S+$).{4,64}">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="password-label"/></label>
            <input type="password" class="rounded form-control" id="password" name="password" required="required" pattern="^(?=.*[0-9])(?=.*[a-zA-Z-_@#!%$^&*])(?=\S+$).{8,64}">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-5">
            <label class=""><fmt:message key="check-password-label"/></label>
            <input type="password" class="rounded form-control" id="confirm_password" name="confirm_password" required="required" pattern="^(?=.*[0-9])(?=.*[a-zA-Z-_@#!%$^&*])(?=\S+$).{8,64}">
        </div>
    </div>
    <h1></h1>
    <div class="row d-flex justify-content-center">
        <div class="col-5 d-flex justify-content-center">
            <input type="submit" class="rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" value="<fmt:message key="registration-button"/>">
        </div>
    </div>
</form>
</body>
</html>
<script type="text/javascript">
    let password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_password");
    function validatePassword(){
        if(password.value != confirm_password.value) {
            if(${language} = "uk"){
                confirm_password.setCustomValidity("Паролі не збігаються");
            }
            else {
                confirm_password.setCustomValidity("Passwords Don't Match");
            }
        } else {
            confirm_password.setCustomValidity('');
        }
    }
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
    addRemoveErrorAttributeListener()
</script>