<%@ page import="controller.command.ActionMap" %>
<%@ page import="javax.swing.*" %>
<%@ page import="model.dao.UserDAO" %>
<%@ page import="model.entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" session="true" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Language" />

<html>
<head>
    <meta charset="UTF-8"/>
    <title>EPAM_Servlet_Project</title>
    <link rel="stylesheet" href="static/css/error.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src="static/js/error.js"></script>
</head>
<body>
<div>
    <div class="container-fluid py-3" style="height: 80px; background-color: yellowgreen;">
        <div class="d-flex flex-column flex-md-row align-items-center" >
            <input onclick="window.location.href='/main?language=${language}'" type="button" class="rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" value="<fmt:message key="main-button"/>">
            <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
                <form>
                    <select class="form-control me-3 py-2 c" style="width: 120px; height: 40px; background-color: wheat; border-color: black; border-width: 2px" name="language" onchange="submit()">
                        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                        <option value="uk" ${language == 'uk' ? 'selected' : ''}>Українська</option>
                    </select>
                </form>
            </nav>
        </div>
    </div>
</div>
<div class="container-fluid py-3">
    <div class="d-flex">
        <div class="container">
            <div class="row">
                <form>
                    <table class="table table-hover">
                        <thead>
                        <tr class="active">
                            <th>
                                <div class="col">
                                    <fmt:message key="table-user-name"/>
                                    <input onclick="window.location.href='/banUser?sort=name&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                    <input onclick="window.location.href='/banUser?sort=nameDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                                </div>
                            </th>
                            <th>
                                <div class="col">
                                    <fmt:message key="table-user-email"/>
                                    <input onclick="window.location.href='/banUser?sort=email&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                    <input onclick="window.location.href='/banUser?sort=emailDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                                </div>

                            </th>
                            <th>
                                <div class="col">
                                    <fmt:message key="table-user-city"/>
                                    <input onclick="window.location.href='/banUser?sort=city&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                    <input onclick="window.location.href='/banUser?sort=cityDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                                </div>
                            </th>
                            <th>
                                <div class="col">
                                    <fmt:message key="table-user-district"/>
                                    <input onclick="window.location.href='/banUser?sort=district&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                    <input onclick="window.location.href='/banUser?sort=districtDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                                </div>
                            </th>
                            <th>
                                <div class="col">
                                    <fmt:message key="table-user-schoolName"/>
                                    <input onclick="window.location.href='/banUser?sort=schoolName&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↑">
                                    <input onclick="window.location.href='/banUser?sort=schoolNameDesc&page=<%=current(request.getParameter("page"))%>'" type="button" class="" value="↓">
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${listUsers}" var="userBan">
                            <tr>
                                <td><c:out value="${userBan.name}" /></td>
                                <td><c:out value="${userBan.email}" /></td>
                                <td><c:out value="${userBan.city}" /></td>
                                <td><c:out value="${userBan.district}" /></td>
                                <td><c:out value="${userBan.schoolName}" /></td>
                                <c:if test="${userBan.role == 'user'}">
                                    <c:if test="${user.role == 'admin'}">
                                    <td>
                                        <input type="button" onclick="window.location.href='/subBanUser?userId=${userBan.id}&page=<%=current(request.getParameter("page"))%>&sort=<%=request.getParameter("sort")%>'" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="ban-button"/>">
                                    <td>
                                    </c:if>
                                </c:if>
                                <c:if test="${userBan.role == 'ban'}">
                                    <c:if test="${user.role == 'admin'}">
                                    <td>
                                        <input type="button" onclick="window.location.href='/subBanUser?userId=${userBan.id}&page=<%=current(request.getParameter("page"))%>&sort=<%=request.getParameter("sort")%>'" class="me-3 py-2" style="background-color: wheat; width: 120px; height: 40px; border-radius: 5px ;" value="<fmt:message key="unban-button"/>">
                                    <td>
                                    </c:if>
                                </c:if>

                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </form>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <input type="button" name="page" value="<<" onclick="window.location.href='/banUser?page=<%=pref(request.getParameter("page"))%>&sort=<%=request.getParameter("sort")%>'"/>
                        <input type="button" name="page" value=">>" onclick="window.location.href='/banUser?page=<%=next(request.getParameter("page"))%>&sort=<%=request.getParameter("sort")%>'"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<%!
    int pref(String page){
        if(page == null ){
            return 1;
        }
        int pageNum = Integer.parseInt(page);
        if(pageNum <= 1)
        {
            return 1;
        }
        return pageNum - 1;
    }
    int next(String page){
        if(page == null){
            return 2;
        }
        int pageNum = Integer.parseInt(page);
        return pageNum + 1;
    }

    int current(String page){
        if(page == null){
            return 1;
        }
        return Integer.parseInt(page);

    }
%>