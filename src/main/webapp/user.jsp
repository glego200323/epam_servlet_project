<%@ page import="controller.command.ActionMap" %>
<%@ page import="javax.swing.*" %>
<%@ page import="model.dao.UserDAO" %>
<%@ page import="model.entity.User" %>
<%@ page import="model.dao.FacultyDAO" %>
<%@ page import="model.dao.RegistrationDAO" %>
<%@ page import="java.net.http.HttpRequest" %>
<%@ page import="org.apache.catalina.Session" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" session="true" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Language" />

<html>
<head>
    <meta charset="UTF-8"/>
    <title>EPAM_Servlet_Project</title>
    <link rel="stylesheet" href="static/css/error.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src="static/js/error.js"></script>
</head>
<body>
<div>
    <div class="container-fluid py-3" style="height: 80px; background-color: yellowgreen;">
        <div class="d-flex flex-column flex-md-row align-items-center" >
            <input onclick="window.location.href='/main?language=${language}'" type="button" class="rounded" style="background-color: wheat; height: 40px; border-radius: 5px ;" value="<fmt:message key="main-button"/>">
            <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
                <form>
                    <select class="form-control me-3 py-2 c" style="width: 120px; height: 40px; background-color: wheat; border-color: black; border-width: 2px" name="language" onchange="submit()">
                        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                        <option value="uk" ${language == 'uk' ? 'selected' : ''}>Українська</option>
                    </select>
                </form>
            </nav>
        </div>
    </div>
</div>
<div class="container-fluid py-3">
    <div class="d-flex">
        <table class="table table-hover">
            <thead>
            <tr class="active">
                <th>
                    <div class="col">
                        <fmt:message key="table-registration-name"/>
                     </div>
                </th>
                <th>
                    <div class="col">
                        <fmt:message key="table-registration-status"/>
                   </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${list}" var="userPage">
                <tr>
                    <td>
                        <c:out value="${userPage.name}"/>
                    </td>
                    <td>
                        <c:if test="${userPage.status == 0}">
                            <fmt:message key="in-process"/>
                        </c:if>
                        <c:if test="${userPage.status == -1}">
                            <fmt:message key="denied"/>
                        </c:if>
                        <c:if test="${userPage.status == 1}">
                            <fmt:message key="accepted"/>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>