package model.service;

import model.dao.Connection.DataBaseConnectionManager;
import model.dao.FacultyDAO;
import model.entity.Faculty;

import java.util.Collections;
import java.util.List;

public class FacultyService {

    private final FacultyDAO facultyDAO =  new FacultyDAO();

    public FacultyDAO getFacultyDAO() {
        return facultyDAO;
    }

    public List<Faculty> getCorrectFaculties(Integer size, Integer page, String sort){
        List<Faculty> faculties = facultyDAO.getAllFaculty();
        if(sort != null){
            switch (sort){
                case "name": faculties = facultyDAO.getAllFacultySortedByName(); break;
                case "nameDesc": faculties = facultyDAO.getAllFacultySortedByNameDesc(); break;
                case "allPlaces": faculties = facultyDAO.getAllFacultySortedByAllPlaces(); break;
                case "allPlacesDesc": faculties = facultyDAO.getAllFacultySortedByAllPlacesDesc(); break;
                case "freePlaces": faculties = facultyDAO.getAllFacultySortedByFreePlaces(); break;
                case "freePlacesDesc": faculties = facultyDAO.getAllFacultySortedByFreePlacesDesc(); break;
            }
        }
        DataBaseConnectionManager.putConnection(facultyDAO.getConnection());
        if(page == null || page <= 0 ){
            page = 1;
        }
        int start = (page-1)*size;
        int end = Math.min(start + size, faculties.size());
        if(start > end){
            return Collections.EMPTY_LIST;
        }
        return faculties.subList(start,end);
    }

    public void deleteFaculty(int facultyId) {
        facultyDAO.deleteById(facultyId);
        DataBaseConnectionManager.putConnection(facultyDAO.getConnection());
    }

    public Faculty getFacultyById(int facultyId) {
        return facultyDAO.findById(facultyId);
    }

    public void save(Faculty faculty) {
        facultyDAO.save(faculty);
        DataBaseConnectionManager.putConnection(facultyDAO.getConnection());
    }

    public void update(Faculty faculty) {
        facultyDAO.updateById(faculty);
        DataBaseConnectionManager.putConnection(facultyDAO.getConnection());
    }
}
