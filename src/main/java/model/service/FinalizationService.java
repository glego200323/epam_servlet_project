package model.service;

import model.dao.FacultyDAO;
import model.dao.RegistrationDAO;
import model.entity.Faculty;
import model.entity.Registration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FinalizationService {

    public void finalizeRegistration(int id) throws SQLException {
        RegistrationDAO registrationDAO = new RegistrationDAO();
        List<Registration> registrations = registrationDAO.getRegistrationForFacultyByFacultyId(id);
        FacultyDAO facultyDAO = new FacultyDAO();
        Faculty faculty = facultyDAO.findById(id);
        List<Pair> pairs = new ArrayList<>();
        for(int i = 0; i < registrations.size();i++){
            pairs.add(new Pair((
                    registrations.get(i).getAverageCertificate() +
                    registrations.get(i).getFirstExam() +
                    registrations.get(i).getSecondExam() +
                    registrations.get(i).getThirdExam()),registrations.get(i).getId()));
        }
        pairs.sort(Comparator.comparing(Pair::getScore));
        List<Integer> success = new ArrayList<>();
        if(pairs.size() <= faculty.getAllPlaces()){
            for(int i=0; i< pairs.size();i++){
                success.add(pairs.get(i).id);
            }
        }else {
            pairs.subList(0,faculty.getAllPlaces());
            for(int i = 0; i < pairs.size(); i++){
                success.add(pairs.get(i).id);
            }
        }
        for(int i = 0; i < registrations.size(); i++){
            if(success.contains(registrations.get(i).getId())){
                registrations.get(i).setStatus(1);
            }
            else {
                registrations.get(i).setStatus(-1);
            }
            registrationDAO.updateById(registrations.get(i));
        }
        registrationDAO.getConnection().close();
    }
    class Pair{
        public Pair(int score,int id){
            this.score = score;
            this.id = id;
        }
        int score;
        public int getScore() {
            return score;
        }
        int id;
    }
}
