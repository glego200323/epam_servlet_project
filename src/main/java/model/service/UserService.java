package model.service;

import model.dao.Connection.DataBaseConnectionManager;
import model.dao.UserDAO;
import model.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

public class UserService {

    private UserDAO userDAO = new UserDAO();

    public User newUser(HttpServletRequest request){
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setEmail(request.getParameter("email"));
        user.setCity(request.getParameter("city"));
        user.setDistrict(request.getParameter("district"));
        user.setSchoolName(request.getParameter("school"));
        user.setLogin(request.getParameter("login"));
        user.setPassword(request.getParameter("password"));
        user.setRole("user");
        return user;
    }
    /**
     * Return true if user is existed.
     **/
    public boolean checkUserLogin(User user){
        boolean value = userDAO.findByLogin(user.getLogin()) != null;
        DataBaseConnectionManager.putConnection(userDAO.getConnection());
        return value;
    }

    public void saveUser(User user){
        userDAO.save(user);
        DataBaseConnectionManager.putConnection(userDAO.getConnection());
    }

    public User authorizationUser(String login,String password){
        User user;
        user = userDAO.findByLogin(login);
        DataBaseConnectionManager.putConnection(userDAO.getConnection());
        if(user != null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }

    public List<User> getCorrectUsers(Integer size, Integer page, String sort){
        List<User> users = userDAO.getAllUser();
        if(sort != null){
            switch (sort){
                case "name": users = userDAO.getAllUserSortedByName(); break;
                case "nameDesc": users = userDAO.getAllUserSortedByNameDesc(); break;
                case "email": users = userDAO.getAllUserSortedByEmail(); break;
                case "emailDesc": users = userDAO.getAllUserSortedByEmailDesc(); break;
                case "city": users = userDAO.getAllUserSortedByCity(); break;
                case "cityDesc": users = userDAO.getAllUserSortedByCityDesc(); break;
                case "district": users = userDAO.getAllUserSortedByDistrict(); break;
                case "districtDesc": users = userDAO.getAllUserSortedByDistrictDesc(); break;
                case "schoolName": users = userDAO.getAllUserSortedBySchoolName(); break;
                case "schoolNameDesc": users = userDAO.getAllUserSortedBySchoolNameDesc(); break;
            }
        }
        DataBaseConnectionManager.putConnection(userDAO.getConnection());
        if(page == null || page <= 0 ){
            page = 1;
        }
        int start = (page-1)*size;
        int end = Math.min(start + size, users.size());
        if(start > end){
            return Collections.EMPTY_LIST;
        }
        return users.subList(start,end);
    }
    public User getUserById(Integer id) {
        return userDAO.findById(id);
    }

    public void update(User user) {
        userDAO.updateById(user);
        DataBaseConnectionManager.putConnection(userDAO.getConnection());
    }
}
