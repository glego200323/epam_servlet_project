package model.entity;

import java.util.Objects;

public class Faculty {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAllPlaces() {
        return allPlaces;
    }

    public void setAllPlaces(Integer allPlaces) {
        this.allPlaces = allPlaces;
    }

    public Integer getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(Integer freePlaces) {
        this.freePlaces = freePlaces;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", allPlaces=" + allPlaces +
                ", freePlaces=" + freePlaces +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Faculty faculty = (Faculty) o;
        return Objects.equals(getName(), faculty.getName()) && Objects.equals(getId(), faculty.getId()) && Objects.equals(getAllPlaces(), faculty.getAllPlaces()) && Objects.equals(getFreePlaces(), faculty.getFreePlaces());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getId(), getAllPlaces(), getFreePlaces());
    }

    private String name;
    private Integer id;
    private Integer allPlaces;
    private Integer freePlaces;
 }
