package model.entity;

import java.util.Objects;

public class User {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getDistrict() {
        return district;
    }
    public void setDistrict(String district) {
        this.district = district;
    }
    public String getSchoolName() {
        return schoolName;
    }
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User user)) return false;
        return Objects.equals(getName(), user.getName()) && Objects.equals(getEmail(), user.getEmail()) && Objects.equals(getCity(), user.getCity()) && Objects.equals(getDistrict(), user.getDistrict()) && Objects.equals(getSchoolName(), user.getSchoolName()) && Objects.equals(getPassword(), user.getPassword()) && Objects.equals(getLogin(), user.getLogin()) && Objects.equals(getRole(), user.getRole()) && Objects.equals(getId(), user.getId());
    }
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getEmail(), getCity(), getDistrict(), getSchoolName(), getPassword(), getLogin(), getRole(), getId());
    }
    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", login='" + login + '\'' +
                ", role='" + role + '\'' +
                ", id=" + id +
                '}';
    }
    String name;
    String email;
    String city;
    String district;
    String schoolName;
    String password;
    String login;
    String role;
    Integer id;
}
