package model.entity;

import java.util.Objects;

public class Registration {

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                ", userId=" + userId +
                ", facultyId=" + facultyId +
                ", firstExam=" + firstExam +
                ", secondExam=" + secondExam +
                ", thirdExam=" + thirdExam +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Registration)) return false;
        Registration that = (Registration) o;
        return id == that.id && userId == that.userId && facultyId == that.facultyId && firstExam == that.firstExam && secondExam == that.secondExam && thirdExam == that.thirdExam && status == that.status && averageCertificate == that.averageCertificate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, facultyId, firstExam, secondExam, thirdExam, status, averageCertificate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int id;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(int facultyId) {
        this.facultyId = facultyId;
    }

    public int getFirstExam() {
        return firstExam;
    }

    public void setFirstExam(int firstExam) {
        this.firstExam = firstExam;
    }

    public int getSecondExam() {
        return secondExam;
    }

    public void setSecondExam(int secondExam) {
        this.secondExam = secondExam;
    }

    public int getThirdExam() {
        return thirdExam;
    }

    public void setThirdExam(int thirdExam) {
        this.thirdExam = thirdExam;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAverageCertificate() {
        return averageCertificate;
    }

    public void setAverageCertificate(int averageCertificate) {
        this.averageCertificate = averageCertificate;
    }

    int userId;
    int facultyId;
    int firstExam;
    int secondExam;
    int thirdExam;
    int status;
    int averageCertificate;
}
