package model.dao;

import model.dao.Connection.DataBaseConnectionManager;
import model.entity.Faculty;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FacultyDAO {
    private static final String INSERT_QUERY = "INSERT INTO faculty (name,all_places,free_places) VALUES (?,?,?)";
    private static final String SELECT_FACULTY_BY_NAME = "SELECT * FROM faculty WHERE name = ?";
    private static final String SELECT_FACULTY_BY_ID = "SELECT * FROM faculty WHERE id = ?";
    private static final String DELETE_BY_NAME = "DELETE FROM faculty WHERE name = ?";
    private static final String DELETE_BY_ID = "DELETE FROM faculty WHERE id = ?";
    private static final String UPDATE_BY_ID = "UPDATE faculty SET all_places=? , free_places=? , name=? WHERE id=?";
    private static final String SELECT_FACULTY_ALL = "SELECT * FROM faculty";
    private static final String SELECT_FACULTY_ALL_SORTED_BY_NAME = "SELECT * FROM faculty ORDER BY name";
    private static final String SELECT_FACULTY_ALL_SORTED_BY_NAME_DESC = "SELECT * FROM faculty ORDER BY name DESC";
    private static final String SELECT_FACULTY_ALL_SORTED_BY_ALL_PLACES = "SELECT * FROM faculty ORDER BY all_places";
    private static final String SELECT_FACULTY_ALL_SORTED_BY_ALL_PLACES_DESC = "SELECT * FROM faculty ORDER BY all_places DESC";
    private static final String SELECT_FACULTY_ALL_SORTED_BY_FREE_PLACES = "SELECT * FROM faculty ORDER BY free_places";
    private static final String SELECT_FACULTY_ALL_SORTED_BY_FREE_PLACES_DESC = "SELECT * FROM faculty ORDER BY free_places DESC";

    private Connection connection;

    public FacultyDAO(){
        this.connection = DataBaseConnectionManager.getConnection();
    }

    public Connection getConnection(){
        return connection;
    }

    public void save(Faculty faculty){
        if(faculty == null){
            return;
        }
        if(!Objects.isNull(findByName(faculty.getName()))){
            return;
        }
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, faculty.getName());
            statement.setInt(2, faculty.getAllPlaces());
            statement.setInt(3, faculty.getFreePlaces());
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()){
                int id = generatedKeys.getInt("id");
                faculty.setId(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * @return Object from DataBase, if not exist null.
     **/
    public Faculty findByName(String name){
        if(name == null){
            return null;
        }
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_FACULTY_BY_NAME);
            statement.setString(1,name);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                return parseFaculty(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    private Faculty parseFaculty(ResultSet set) throws SQLException {
        Faculty tmp = new Faculty();
        tmp.setName(set.getString("name"));
        tmp.setAllPlaces(set.getInt("all_places"));
        tmp.setFreePlaces(set.getInt("free_places"));
        tmp.setId(set.getInt("id"));
        return tmp;
    }
    public Faculty findById(Integer id){
        if(id == null){
            return null;
        }
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_FACULTY_BY_ID);
            statement.setInt(1,id);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                return parseFaculty(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteByName(String name){
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(DELETE_BY_NAME);
            statement.setString(1,name);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateById(Faculty faculty){
        if(faculty.getId() == null){
            return;
        }
        if(findById(faculty.getId()) == null){
            return;
        }
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_BY_ID);
            statement.setInt(1,faculty.getAllPlaces());
            statement.setInt(2,faculty.getFreePlaces());
            statement.setString(3,faculty.getName());
            statement.setInt(4,faculty.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Faculty> getAllFaculty(){
        return getFaculties(SELECT_FACULTY_ALL);
    }
    public List<Faculty> getAllFacultySortedByName(){
        return getFaculties(SELECT_FACULTY_ALL_SORTED_BY_NAME);
    }
    public List<Faculty> getAllFacultySortedByNameDesc(){
        return getFaculties(SELECT_FACULTY_ALL_SORTED_BY_NAME_DESC);
    }
    public List<Faculty> getAllFacultySortedByAllPlaces(){
        return getFaculties(SELECT_FACULTY_ALL_SORTED_BY_ALL_PLACES);
    }
    public List<Faculty> getAllFacultySortedByAllPlacesDesc(){
        return getFaculties(SELECT_FACULTY_ALL_SORTED_BY_ALL_PLACES_DESC);
    }
    public List<Faculty> getAllFacultySortedByFreePlaces(){
        return getFaculties(SELECT_FACULTY_ALL_SORTED_BY_FREE_PLACES);
    }
    public List<Faculty> getAllFacultySortedByFreePlacesDesc(){
        return getFaculties(SELECT_FACULTY_ALL_SORTED_BY_FREE_PLACES_DESC);
    }

    @NotNull
    private List<Faculty> getFaculties(String selectFaculty) {
        List<Faculty> faculties = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(selectFaculty);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                faculties.add(parseFaculty(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return faculties;
    }

    public void deleteById(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(DELETE_BY_ID);
            statement.setInt(1,id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
