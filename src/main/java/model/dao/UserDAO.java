package model.dao;

import model.dao.Connection.DataBaseConnectionManager;
import model.entity.User;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserDAO {

    private static final String INSERT_QUERY = "INSERT INTO users (name,email,city,district,schoolName,password,login,role) VALUES (?,?,?,?,?,?,?,?)";
    private static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
    private static final String SELECT_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
    private static final String SELECT_ALL_USER = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin'";
    private static final String SELECT_ALL_USER_SORTED_BY_NAME = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY name";
    private static final String SELECT_ALL_USER_SORTED_BY_NAME_DESC = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY name DESC";
    private static final String SELECT_ALL_USER_SORTED_BY_EMAIL = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY email";
    private static final String SELECT_ALL_USER_SORTED_BY_EMAIL_DESC = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY email DESC";
    private static final String SELECT_ALL_USER_SORTED_BY_CITY = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY city";
    private static final String SELECT_ALL_USER_SORTED_BY_CITY_DESC = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY city DESC";
    private static final String SELECT_ALL_USER_SORTED_BY_DISTRICT = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY district";
    private static final String SELECT_ALL_USER_SORTED_BY_DISTRICT_DESC = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY district DESC";
    private static final String SELECT_ALL_USER_SORTED_BY_SCHOOL_NAME = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY schoolname";
    private static final String SELECT_ALL_USER_SORTED_BY_SCHOOL_NAME_DESC = "SELECT * FROM users INTERSECT SELECT * FROM users WHERE role != 'admin' ORDER BY schoolname DESC";
    private static final String UPDATE_BY_ID = "UPDATE users SET name=? , email = ? , city = ? , district = ? , schoolname = ? , role = ? WHERE id=?";
    private Connection connection;

    public UserDAO(){
        this.connection = DataBaseConnectionManager.getConnection();
    }
    public Connection getConnection(){
        return connection;
    }
    public void save(User user){
        if(user == null){
            return;
        }
        if(!Objects.isNull(findByLogin(user.getLogin()))){
            return;
        }
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getCity());
            statement.setString(4, user.getDistrict());
            statement.setString(5, user.getSchoolName());
            statement.setString(6, user.getPassword());
            statement.setString(7, user.getLogin());
            statement.setString(8, user.getRole());
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()){
                int id = generatedKeys.getInt("id");
                user.setId(id);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public User findByLogin(String login){
        if(login == null){
            return null;
        }
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_USER_BY_LOGIN);
            statement.setString(1,login);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                return parseUser(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    private User parseUser(ResultSet set) throws SQLException {
        User tmp = new User();
        tmp.setName(set.getString("name"));
        tmp.setEmail(set.getString("email"));
        tmp.setCity(set.getString("city"));
        tmp.setDistrict(set.getString("district"));
        tmp.setSchoolName(set.getString("schoolName"));
        tmp.setPassword(set.getString("password"));
        tmp.setLogin(set.getString("login"));
        tmp.setRole(set.getString("role"));
        tmp.setId(set.getInt("id"));
        return tmp;
    }
    public void deleteByID(int id){
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(DELETE_USER_BY_ID);
            statement.setInt(1,id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @NotNull
    private List<User> getUsers(String selectUser) {
        List<User> faculties = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(selectUser);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                faculties.add(parseUser(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return faculties;
    }
    public List<User> getAllUser() {
        return getUsers(SELECT_ALL_USER);
    }
    public List<User> getAllUserSortedByName() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_NAME);
    }
    public List<User> getAllUserSortedByNameDesc() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_NAME_DESC);
    }
    public List<User> getAllUserSortedByEmail() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_EMAIL);
    }
    public List<User> getAllUserSortedByEmailDesc() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_EMAIL_DESC);
    }
    public List<User> getAllUserSortedByCity() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_CITY);
    }
    public List<User> getAllUserSortedByCityDesc() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_CITY_DESC);
    }
    public List<User> getAllUserSortedByDistrict() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_DISTRICT);
    }
    public List<User> getAllUserSortedByDistrictDesc() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_DISTRICT_DESC);
    }
    public List<User> getAllUserSortedBySchoolName() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_SCHOOL_NAME);
    }
    public List<User> getAllUserSortedBySchoolNameDesc() {
        return getUsers(SELECT_ALL_USER_SORTED_BY_SCHOOL_NAME_DESC);
    }

    public User findById(Integer id) {
        if(id == null){
            return null;
        }
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_USER_BY_ID);
            statement.setInt(1,id);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                return parseUser(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void updateById(User user){
        if(user.getId() == null){
            return;
        }
        if(findById(user.getId()) == null){
            return;
        }
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_BY_ID);
            statement.setString(1,user.getName());
            statement.setString(2,user.getEmail());
            statement.setString(3,user.getCity());
            statement.setString(4,user.getDistrict());
            statement.setString(5,user.getSchoolName());
            statement.setString(6,user.getRole());
            statement.setInt(7,user.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
