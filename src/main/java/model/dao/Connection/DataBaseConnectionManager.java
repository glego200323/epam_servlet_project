package model.dao.Connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataBaseConnectionManager{
    static Properties property = new Properties();
    private static ConnectionPool connectionPool;
    static {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("app.properties");
        try {
            property.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Class.forName("org.postgresql.Driver");
            connectionPool = BasicConnectionPool.create(property.getProperty("url"),
                                                        property.getProperty("user"),
                                                        property.getProperty("password"));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        Connection connection;
        connection = connectionPool.getConnection();
        return connection;
    }

    public static void putConnection(Connection connection){
        connectionPool.releaseConnection(connection);
    }
    private DataBaseConnectionManager(){}
}
