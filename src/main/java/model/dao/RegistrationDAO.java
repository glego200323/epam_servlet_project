package model.dao;

import model.dao.Connection.DataBaseConnectionManager;
import model.entity.Registration;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RegistrationDAO {

    private static final String INSERT_QUERY = "INSERT INTO users_registration (user_id,faculty_id,first_exam,second_exam,third_exam,status,average_certificate) VALUES (?,?,?,?,?,?,?)";
    private static final String SELECT_REGISTRATION_ALL = "SELECT * FROM users_registration";
    private static final String SELECT_REGISTRATION_BY_FACULTY_ID = "SELECT * FROM users_registration WHERE faculty_id = ?";
    private static final String UPDATE_BY_ID = "UPDATE users_registration SET user_id=? , faculty_id = ? , first_exam = ? , second_exam = ? , third_exam = ? , status = ?, average_certificate = ? WHERE id=?";
    private static final String SELECT_USER_PAGE_BY_USER_ID = "SELECT faculty.name, users_registration.status FROM faculty INNER JOIN users_registration ON faculty.id = users_registration.faculty_id WHERE users_registration.user_id=?";
    private static final String SELECT_FACULTY_ID_BY_USER_ID = "SELECT faculty_id FROM users_registration WHERE user_id = ?";

    private Connection connection;
    public RegistrationDAO(){
        this.connection = DataBaseConnectionManager.getConnection();
    }
    public Connection getConnection(){
        return connection;
    }

    public List<Registration> getAllRegistration(){
        return getRegistration(SELECT_REGISTRATION_ALL);
    }
    @NotNull
    public List<Registration> getRegistration(String selectRegistrationAllSortedByName){
            List<Registration> registrations = new ArrayList<>();
            PreparedStatement statement = null;
            try {
                statement = connection.prepareStatement(selectRegistrationAllSortedByName);
                ResultSet set = statement.executeQuery();
                while (set.next()){
                    registrations.add(parseRegistration(set));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return registrations;
    }

    public List<Integer> getUserFacultyIdByUserId(Integer id){
        List<Integer> list = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_FACULTY_ID_BY_USER_ID);
            statement.setInt(1,id);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                list.add(set.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void save(Registration registration){
        if(registration == null){
            return;
        }
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, registration.getUserId());
            statement.setInt(2, registration.getFacultyId());
            statement.setInt(3, registration.getFirstExam());
            statement.setInt(4, registration.getSecondExam());
            statement.setInt(5, registration.getThirdExam());
            statement.setInt(6, registration.getStatus());
            statement.setInt(7, registration.getAverageCertificate());
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()){
                int id = generatedKeys.getInt("id");
                registration.setId(id);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private Registration parseRegistration(ResultSet set) throws SQLException {
        Registration tmp = new Registration();
        tmp.setId(set.getInt("id"));
        tmp.setUserId(set.getInt("user_id"));
        tmp.setFacultyId(set.getInt("faculty_id"));
        tmp.setFirstExam(set.getInt("first_exam"));
        tmp.setSecondExam(set.getInt("second_exam"));
        tmp.setThirdExam(set.getInt("third_exam"));
        tmp.setStatus(set.getInt("status"));
        tmp.setAverageCertificate(set.getInt("average_certificate"));
        return tmp;
    }

    public List<Registration> getRegistrationForFacultyByFacultyId(int id){
        List<Registration> list = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_REGISTRATION_BY_FACULTY_ID);
            statement.setInt(1,id);
            ResultSet set = statement.executeQuery();
            while (set.next()){
               list.add(parseRegistration(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<UserPage> getUserPageByUserId(int id){
        List<UserPage> list = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SELECT_USER_PAGE_BY_USER_ID);
            statement.setInt(1,id);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                list.add(parseUserPage(set));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void updateById(Registration registration){
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UPDATE_BY_ID);
            statement.setInt(1,registration.getUserId());
            statement.setInt(2,registration.getFacultyId());
            statement.setInt(3,registration.getFirstExam());
            statement.setInt(4,registration.getSecondExam());
            statement.setInt(5,registration.getThirdExam());
            statement.setInt(6,registration.getStatus());
            statement.setInt(7,registration.getAverageCertificate());
            statement.setInt(8,registration.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public UserPage parseUserPage(ResultSet set) throws SQLException {
        UserPage tmp = new UserPage();
        tmp.setName(set.getString("name"));
        tmp.setStatus(set.getInt("status"));
        return tmp;
    }
    public static class UserPage{
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public int getStatus() {
            return status;
        }
        public void setStatus(int status) {
            this.status = status;
        }
        public String name;
        public int status;
    }
}