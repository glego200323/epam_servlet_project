package controller;


import controller.command.ActionMap;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet("/")
public class
Controller extends HttpServlet {
    public static final Logger logger = Logger.getLogger(Controller.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            processRequest(req, resp);
            logger.info("Get");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            processRequest(req, resp);
            logger.info("Post");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException, SQLException {
        String url = req.getRequestURI();
        ActionMap.get(url).execute(req,resp);
    }
}
