package controller.command.impl;

import controller.command.Action;
import model.dao.RegistrationDAO;
import model.entity.User;
import model.service.FacultyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class MainPageCommand implements Action {
    int DEFAULT_SIZE_OF_PAGINATION = 12;
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        request.setAttribute("list", new FacultyService().getCorrectFaculties(DEFAULT_SIZE_OF_PAGINATION,Integer.valueOf(Optional.ofNullable(request.getParameter("page")).orElse("1")), request.getParameter("sort")));
        User user = (User) request.getSession().getAttribute("user");
        if(user != null){
            request.setAttribute("userFacultyIdList",new RegistrationDAO().getUserFacultyIdByUserId(user.getId()));
        }
        request.getRequestDispatcher("/main.jsp").forward(request,response);
    }
}
