package controller.command.impl;

import controller.command.Action;
import model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class AuthorizationCommand implements Action {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        request.getSession().setAttribute("user",new UserService().authorizationUser(request.getParameter("authLogin"),request.getParameter("authPassword")));
        response.sendRedirect(request.getParameter("viewed"));
    }
}
