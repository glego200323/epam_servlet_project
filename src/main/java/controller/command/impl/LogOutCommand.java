package controller.command.impl;

import controller.command.Action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class LogOutCommand implements Action {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        request.getSession().setAttribute("user", null);
        response.sendRedirect("/main?language=" + request.getSession().getAttribute("language"));
    }
}
