package controller.command.impl.user;

import controller.command.Action;
import model.dao.RegistrationDAO;
import model.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class UserAccountPageCommand implements Action{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        User user = (User) request.getSession().getAttribute("user");
        if(!user.getRole().equals("user")){
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }else {
            RegistrationDAO registrationDAO = new RegistrationDAO();
            List<RegistrationDAO.UserPage> userPage = registrationDAO.getUserPageByUserId(user.getId());
            request.setAttribute("list", userPage);
            System.out.println(userPage);
            request.getRequestDispatcher("/user.jsp").forward(request,response);
        }
    }
}
