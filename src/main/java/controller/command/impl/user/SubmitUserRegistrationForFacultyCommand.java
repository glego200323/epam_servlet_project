package controller.command.impl.user;

import controller.command.Action;
import model.dao.RegistrationDAO;
import model.entity.Registration;
import model.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class SubmitUserRegistrationForFacultyCommand implements Action {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        User user = (User) request.getSession().getAttribute("user");
        if(!user.getRole().equals("user")){
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }else {
            RegistrationDAO registrationDAO = new RegistrationDAO();

            System.out.println(registrationDAO.getUserFacultyIdByUserId(user.getId()));

            if(!registrationDAO.getUserFacultyIdByUserId(user.getId())
                    .contains(Integer.parseInt(request.getParameter("facultyId")))){
                Registration registration = new Registration();
                registration.setFacultyId(Integer.parseInt(request.getParameter("facultyId")));
                registration.setUserId(user.getId());
                registration.setFirstExam(Integer.parseInt(request.getParameter("first_exam")));
                registration.setSecondExam(Integer.parseInt(request.getParameter("second_exam")));
                registration.setThirdExam(Integer.parseInt(request.getParameter("third_exam")));
                registration.setAverageCertificate(Integer.parseInt(request.getParameter("average_certificate")));
                registration.setStatus(0);
                registrationDAO.save(registration);
            }
            request.getRequestDispatcher("/main").forward(request,response);
        }
    }
}
