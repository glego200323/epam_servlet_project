package controller.command.impl.user.admin;

import controller.command.Action;
import model.entity.User;
import model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class BanUserPageCommand implements Action {
    int DEFAULT_SIZE_OF_PAGINATION = 12;
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        User user = (User) request.getSession().getAttribute("user");
        if(user == null || (!user.getRole().equals("admin"))){
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }else {
            request.setAttribute("listUsers",
                                    new UserService().getCorrectUsers(DEFAULT_SIZE_OF_PAGINATION,
                                    Integer.valueOf(Optional.ofNullable(request.getParameter("page")).orElse("1")),
                                    (request.getParameter("sort") == null ? "name":request.getParameter("sort"))));
            request.getRequestDispatcher("/banUser.jsp").forward(request,response);
        }
    }
}
