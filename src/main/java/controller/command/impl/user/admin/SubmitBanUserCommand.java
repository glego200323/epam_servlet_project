package controller.command.impl.user.admin;

import controller.command.Action;
import model.entity.User;
import model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class SubmitBanUserCommand implements Action {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        User user = (User) request.getSession().getAttribute("user");
        if(user == null || (!user.getRole().equals("admin"))){
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }else {
            UserService userService = new UserService();
            User userBan = userService.getUserById(Integer.valueOf(request.getParameter("userId")));
            if(userBan.getRole().equals("ban")){
                userBan.setRole("user");
            }else {
                userBan.setRole("ban");
            }
            userService.update(userBan);
            response.sendRedirect("/banUser?page=" + request.getParameter("page") + "&sort=" + (request.getParameter("sort") == null ? "name":request.getParameter("sort")));
        }
    }
}
