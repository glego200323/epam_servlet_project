package controller.command.impl.user;

import controller.command.Action;
import model.entity.Faculty;
import model.entity.User;
import model.service.FacultyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UserRegistrationForFacultyCommand implements Action {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        User user = (User) request.getSession().getAttribute("user");
        if(!user.getRole().equals("user")){
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }else {
            FacultyService facultyService = new FacultyService();
            Faculty faculty = facultyService.getFacultyById(Integer.parseInt(request.getParameter("facultyId")));
            request.setAttribute("faculty",faculty);
            request.getRequestDispatcher("/registrationForFaculty.jsp").forward(request,response);
        }
    }
}
