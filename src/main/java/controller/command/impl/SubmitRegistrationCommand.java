package controller.command.impl;

import controller.command.Action;
import model.entity.User;
import model.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class SubmitRegistrationCommand implements Action {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        UserService userService = new UserService();
        User user = userService.newUser(request);
        if(userService.checkUserLogin(user)){
            request.setAttribute("loginError","already exist");
            request.getRequestDispatcher("/reg?language=" + request.getSession().getAttribute("language")).forward(request,response);
        }else {
            userService.saveUser(user);
            response.sendRedirect("/reg?language=" + request.getSession().getAttribute("language"));
        }
    }
}
