package controller.command;

import controller.command.impl.Error;
import controller.command.impl.user.SubmitUserRegistrationForFacultyCommand;
import controller.command.impl.user.UserAccountPageCommand;
import controller.command.impl.user.UserRegistrationForFacultyCommand;
import controller.command.impl.*;
import controller.command.impl.user.admin.*;

import java.util.HashMap;
import java.util.Map;

public class ActionMap {
    private static final Map<String, Action> actionMap = new HashMap<>();
    static {
        actionMap.put("/main",new MainPageCommand());
        actionMap.put("/reg",new RegistrationPageCommand());
        actionMap.put("/regUser",new SubmitRegistrationCommand());
        actionMap.put("/auth",new AuthorizationCommand());
        actionMap.put("/user",new UserAccountPageCommand());
        actionMap.put("/userRegFaculty",new UserRegistrationForFacultyCommand());
        actionMap.put("/error",new Error());
        actionMap.put("/admin",new AdminPageCommand());
        actionMap.put("/addFac",new FacultyAddPageCommand());
        actionMap.put("/delFac",new FacultyDeletePageCommand());
        actionMap.put("/logOut",new LogOutCommand());
        actionMap.put("/subEdit",new SubmitFacultyEditCommand());
        actionMap.put("/edit",new FacultyEditPageCommand());
        actionMap.put("/add",new FacultyAddPageCommand());
        actionMap.put("/subAdd",new SubmitFacultyAddCommand());
        actionMap.put("/banUser",new BanUserPageCommand());
        actionMap.put("/subBanUser",new SubmitBanUserCommand());
        actionMap.put("/subRegForFac",new SubmitUserRegistrationForFacultyCommand());
        actionMap.put("/final",new FinalizeCommand());
    }
    public static Action get(String str){
        if( actionMap.get(str) != null){
            return  actionMap.get(str);
        }
        return  actionMap.get("/error");
    }
    private ActionMap(){}
}
