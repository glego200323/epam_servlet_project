package dao;

import model.dao.FacultyDAO;
import model.entity.Faculty;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FacultyDAOTest {
    FacultyDAO facultyDAO = new FacultyDAO();
    @Test
    public void findByName() {
        Faculty tmp = new Faculty();
        tmp.setName("TEST");
        tmp.setFreePlaces(10);
        tmp.setAllPlaces(10);
        facultyDAO.save(tmp);
        Faculty tmp2 = facultyDAO.findByName("TEST");
        Assert.assertNotNull(tmp2);
        facultyDAO.deleteByName("TEST");
    }

    @Test
    public void save() {
        Faculty tmp = new Faculty();
        tmp.setName("TEST");
        tmp.setFreePlaces(10);
        tmp.setAllPlaces(10);
        facultyDAO.save(tmp);
        Faculty tmp2 = facultyDAO.findByName(tmp.getName());
        facultyDAO.deleteByName(tmp.getName());
        Assert.assertNotNull(tmp2);
        Assert.assertEquals(tmp,tmp2);
    }

    @Test
    public void deleteByName() {
        Faculty tmp = new Faculty();
        tmp.setName("TEST");
        tmp.setFreePlaces(10);
        tmp.setAllPlaces(10);
        facultyDAO.save(tmp);
        facultyDAO.deleteByName(tmp.getName());
        Faculty tmp2 = facultyDAO.findByName(tmp.getName());
        Assert.assertNull(tmp2);
    }

    @Test
    public void findById() {
        Faculty tmp = new Faculty();
        tmp.setName("TEST");
        tmp.setFreePlaces(10);
        tmp.setAllPlaces(10);
        facultyDAO.save(tmp);
        Faculty tmp2 = facultyDAO.findByName("TEST");
        Faculty tmp3 = facultyDAO.findById(tmp2.getId());
        Assert.assertNotNull(tmp3);
        facultyDAO.deleteByName("TEST");
    }

    @Test
    public void updateById() {
        Faculty tmp = new Faculty();
        tmp.setName("TEST");
        tmp.setFreePlaces(10);
        tmp.setAllPlaces(10);
        facultyDAO.save(tmp);
        Faculty tmp2 = facultyDAO.findByName("TEST");
        tmp2.setFreePlaces(20);
        tmp2.setAllPlaces(20);
        facultyDAO.updateById(tmp2);
        Faculty tmp3 = facultyDAO.findByName("TEST");
        assertTrue(20 == tmp3.getAllPlaces());
        assertTrue(20 == tmp3.getFreePlaces());
        facultyDAO.deleteByName("TEST");
    }
}