package dao;

import model.dao.UserDAO;
import model.entity.User;
import org.junit.Assert;
import org.junit.Test;

public class RegistrationTest {
    UserDAO userDAO = new UserDAO();
    @Test
    public void test1(){
        User user = new User();
        String login = "test1";
        user.setLogin(login);
        String password = "123Test12";
        user.setPassword(password);
        userDAO.save(user);
        User user1 = userDAO.findByLogin(login);
        Assert.assertEquals(user1,user);
        userDAO.deleteByID(user.getId());
    }
}
